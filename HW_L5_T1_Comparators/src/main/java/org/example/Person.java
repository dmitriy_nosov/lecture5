package org.example;

import java.util.Locale;
import java.util.Objects;

public class Person implements Comparable<Person> {

    private Integer age;
    private String name;
    private String city;

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public Person(Integer age, String name, String city) {
        if (city == null) {
            throw new IllegalArgumentException("city cannot be null");
        }
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }
        if (age == null) {
            throw new IllegalArgumentException("age cannot be null");
        }

        this.age = age;
        this.name = name;
        this.city = city;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age='" + age + '\'' +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    @Override
    public int compareTo(Person o) {
        int result = this.city.compareTo(o.getCity());
        if (result == 0) {
            result = this.name.compareTo(o.getName());
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && name.equalsIgnoreCase(person.name) && city.equalsIgnoreCase(person.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, name.toUpperCase(), city.toUpperCase());
    }
}