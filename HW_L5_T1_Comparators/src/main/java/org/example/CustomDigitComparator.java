package org.example;

import java.util.Comparator;

public class CustomDigitComparator implements Comparator<Number> {

    @Override
    public int compare(Number o1, Number o2) {
        if (o1 == null || o2 == null) {
            throw new IllegalArgumentException("arguments cannot be null");
        }
        if (o1.intValue() % 2 == 0) {
            return 1;
        }
        if (o2.intValue() % 2 == 0) {
            return -1;
        }
        return 0;
    }
}