package org.example;

import org.junit.Assert;
import org.junit.Test;

public class CustomDigitComparatorTest {
    /**
     * Comparing digits for parity
     */
    @Test
    public void compareDigitsForParityTest() {
        CustomDigitComparator comparator = new CustomDigitComparator();

        Assert.assertEquals(-1, comparator.compare(21, 212));
    }

    /**
     * Comparing equal digits
     */
    @Test
    public void compareEqualDigitsTest() {
        CustomDigitComparator comparator = new CustomDigitComparator();

        Assert.assertEquals(1, comparator.compare(22, 22L));
    }

    /**
     * Comparing equal digits
     */
    @Test
    public void compareNotEqualDigitsTest() {
        CustomDigitComparator comparator = new CustomDigitComparator();

        Assert.assertEquals(-1, comparator.compare(13, 22));
    }

    /**
     * Comparing null-argument digit 1
     */
    @Test (expected = IllegalArgumentException.class)
    public void compareNullFirstArgumentTest() {
        CustomDigitComparator comparator = new CustomDigitComparator();
        Assert.assertEquals(1, comparator.compare(null, 1));
    }

    /**
     * Comparing null-argument digit 2
     */
    @Test (expected = IllegalArgumentException.class)
    public void compareNullSecondArgumentTest() {
        CustomDigitComparator comparator = new CustomDigitComparator();
        Assert.assertEquals(1, comparator.compare(1, null));
    }
}