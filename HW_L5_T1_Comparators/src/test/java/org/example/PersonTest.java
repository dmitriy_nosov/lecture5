package org.example;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PersonTest {

    /**
     * Comparing Cities for difference (lhs > rhs)
     */
    @Test
    public void compareToDifferenceCityTest() {
        Person person1 = new Person(32, "Dmitriy", "Yekaterinburg");
        Person person2 = new Person(32, "Dmitriy", "Moscow");

        int actual = person1.compareTo(person2);
        Assert.assertTrue(actual > 0);
    }

    /**
     * Comparing Cities for difference (lhs < rhs)
     */
    @Test
    public void compareToDifferenceCity2Test() {
        Person person1 = new Person(32, "Dmitriy", "Moscow");
        Person person2 = new Person(32, "Dmitriy", "Yekaterinburg");

        int actual = person2.compareTo(person1);
        Assert.assertTrue(actual > 0);
    }

    /**
     * Comparing Cities for difference (different names lhs < rhs)
     */
    @Test
    public void compareToDifferenceEqualsCitiesWithDifferentNames1Test() {
        Person person1 = new Person(32, "Dima", "Yekaterinburg");
        Person person2 = new Person(32, "Dmitriy", "Yekaterinburg");

        int actual = person2.compareTo(person1);
        Assert.assertTrue(actual > 0);
    }

    /**
     * Comparing Cities for difference (different names lhs > rhs)
     */
    @Test
    public void compareToDifferenceEqualsCitiesWithDifferentNames2Test() {
        Person person1 = new Person(32, "Dmitriy", "Yekaterinburg");
        Person person2 = new Person(32, "Dima", "Yekaterinburg");

        int actual = person1.compareTo(person2);
        Assert.assertTrue(actual > 0);
    }

    /**
     * Comparing Cities for difference (same names)
     */
    @Test
    public void compareToDifferenceEqualsCitiesWithDifferentNames3Test() {
        Person person1 = new Person(32, "Dima", "Yekaterinburg");
        Person person2 = new Person(32, "Dima", "Yekaterinburg");

        int actual = person1.compareTo(person2);
        assertEquals(0, actual);
    }
}