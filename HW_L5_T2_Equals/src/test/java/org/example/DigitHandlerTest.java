package org.example;

import org.junit.Assert;
import org.junit.Test;

public class DigitHandlerTest {
    /**
     * Test object for equality
     */
    @Test
    public void equalsTest() {
        DigitHandler value = new DigitHandler(5);
        Object newValue = value;

        Assert.assertEquals(newValue, value);
    }

    /**
     * Test different class objects for equality
     */
    @Test
    public void equalsDifferentClassTest() {
        DigitHandler value = new DigitHandler(5);
        Object newValue = 3;

        Assert.assertNotEquals(newValue.getClass(), value.getClass());
    }

    /**
     * Test null object for equality
     */
    @Test
    public void equalsNullObjectTest() {
        DigitHandler value = new DigitHandler(5);

        Assert.assertNotEquals(null, value);
    }

    /**
     * Test objects with same types for equality
     */
    @Test
    public void equalsSameTypeObjectTest() {
        DigitHandler value = new DigitHandler(5);
        Object newValue = new DigitHandler(5);

        Assert.assertEquals(newValue, value);
    }
}