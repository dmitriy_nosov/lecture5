package org.example;

import org.junit.Assert;
import org.junit.Test;

public class PersonEqualsTest {

    /**
     * Check Person for correct name
     */
    @Test (expected = IllegalArgumentException.class)
    public void equalsNullNamePersonTest() {
        Person person1 = new Person(32, null, "Yekaterinburg");
    }

    /**
     * Check Person for correct age
     */
    @Test (expected = IllegalArgumentException.class)
    public void equalsNullAgePersonTest() {
        Person person1 = new Person(null, "Dmitriy", "Yekaterinburg");
    }

    /**
     * Check Person for correct city
     */
    @Test (expected = IllegalArgumentException.class)
    public void equalsNullCityPersonTest() {
        Person person1 = new Person(32, "Dmitriy", null);
    }

    /**
     * Comparing persons for equality
     */
    @Test
    public void equalsPersonTest() {
        Person person1 = new Person(32, "Dmitriy", "Yekaterinburg");
        Person person2 = new Person(32, "Dmitriy", "Yekaterinburg");

        Assert.assertEquals(person1, person2);
    }

    /**
     * Comparing persons for equality ignoring case in name
     */
    @Test
    public void equalsPersonNameIgnoredCaseTest() {
        Person person1 = new Person(32, "dmiTriY", "Yekaterinburg");
        Person person2 = new Person(32, "Dmitriy", "Yekaterinburg");

        Assert.assertEquals(person1, person2);
    }

    /**
     * Comparing persons for equality ignoring case in name
     */
    @Test
    public void equalsCityNameIgnoredCaseTest() {
        Person person1 = new Person(32, "Dmitriy", "yekAterInbuRg");
        Person person2 = new Person(32, "Dmitriy", "Yekaterinburg");

        Assert.assertEquals(person1, person2);
    }
}